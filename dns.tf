# workaround to keep the zone even if all other records are deleted
resource "scaleway_domain_record" "keep" {
  dns_zone = local.dns_root
  name     = ""
  type     = "TXT"
  data     = "keep"
}

resource "scaleway_domain_record" "spf" {
  dns_zone = local.dns_root
  type     = "TXT"
  data     = "v=spf1 ${scaleway_tem_domain.metabase.spf_config} -all"
}

resource "scaleway_domain_record" "dkim" {
  dns_zone = local.dns_root
  name     = "${scaleway_tem_domain.metabase.project_id}._domainkey"
  type     = "TXT"
  data     = scaleway_tem_domain.metabase.dkim_config
}

resource "scaleway_domain_record" "mx" {
  dns_zone = local.dns_root
  type     = "MX"
  data     = scaleway_tem_domain.metabase.mx_blackhole
}

resource "scaleway_domain_record" "dmarc" {
  dns_zone = local.dns_root
  type     = "TXT"
  name     = "_dmarc"
  data     = scaleway_tem_domain.metabase.dmarc_config
}
