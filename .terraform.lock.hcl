# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/null" {
  version = "3.2.3"
  hashes = [
    "h1:+AnORRgFbRO6qqcfaQyeX80W0eX3VmjadjnUFUJTiXo=",
    "zh:22d062e5278d872fe7aed834f5577ba0a5afe34a3bdac2b81f828d8d3e6706d2",
    "zh:23dead00493ad863729495dc212fd6c29b8293e707b055ce5ba21ee453ce552d",
    "zh:28299accf21763ca1ca144d8f660688d7c2ad0b105b7202554ca60b02a3856d3",
    "zh:55c9e8a9ac25a7652df8c51a8a9a422bd67d784061b1de2dc9fe6c3cb4e77f2f",
    "zh:756586535d11698a216291c06b9ed8a5cc6a4ec43eee1ee09ecd5c6a9e297ac1",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:9d5eea62fdb587eeb96a8c4d782459f4e6b73baeece4d04b4a40e44faaee9301",
    "zh:a6355f596a3fb8fc85c2fb054ab14e722991533f87f928e7169a486462c74670",
    "zh:b5a65a789cff4ada58a5baffc76cb9767dc26ec6b45c00d2ec8b1b027f6db4ed",
    "zh:db5ab669cf11d0e9f81dc380a6fdfcac437aea3d69109c7aef1a5426639d2d65",
    "zh:de655d251c470197bcbb5ac45d289595295acb8f829f6c781d4a75c8c8b7c7dd",
    "zh:f5c68199f2e6076bce92a12230434782bf768103a427e9bb9abee99b116af7b5",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version     = "3.6.3"
  constraints = "~> 3.6"
  hashes = [
    "h1:Fnaec9vA8sZ8BXVlN3Xn9Jz3zghSETIKg7ch8oXhxno=",
    "zh:04ceb65210251339f07cd4611885d242cd4d0c7306e86dda9785396807c00451",
    "zh:448f56199f3e99ff75d5c0afacae867ee795e4dfda6cb5f8e3b2a72ec3583dd8",
    "zh:4b4c11ccfba7319e901df2dac836b1ae8f12185e37249e8d870ee10bb87a13fe",
    "zh:4fa45c44c0de582c2edb8a2e054f55124520c16a39b2dfc0355929063b6395b1",
    "zh:588508280501a06259e023b0695f6a18149a3816d259655c424d068982cbdd36",
    "zh:737c4d99a87d2a4d1ac0a54a73d2cb62974ccb2edbd234f333abd079a32ebc9e",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:a357ab512e5ebc6d1fda1382503109766e21bbfdfaa9ccda43d313c122069b30",
    "zh:c51bfb15e7d52cc1a2eaec2a903ac2aff15d162c172b1b4c17675190e8147615",
    "zh:e0951ee6fa9df90433728b96381fb867e3db98f66f735e0c3e24f8f16903f0ad",
    "zh:e3cdcb4e73740621dabd82ee6a37d6cfce7fee2a03d8074df65086760f5cf556",
    "zh:eff58323099f1bd9a0bec7cb04f717e7f1b2774c7d612bf7581797e1622613a0",
  ]
}

provider "registry.terraform.io/scaleway/scaleway" {
  version     = "2.47.0"
  constraints = "~> 2.38"
  hashes = [
    "h1:nmFakn8p0lWWrwFAdjCAu38TFiHCZ1fSSPsogWPLpfA=",
    "zh:12e988e4db022419f38c7b85b16cba359be2104283c09e98a62dcf955e4b8678",
    "zh:4585d0918ba79cf714a839ee34a6a6a2d19be4c21a1893343cb2f8fa3fe537c0",
    "zh:489a496c91c93d2ddfb227adac0e480f3ce3b34fccbc78d357aa3aea8f89b96b",
    "zh:53858ee0f85fa6837d8305f411ff42c9d1951939469f6718adb44894f8e0e1d8",
    "zh:59ec9dee6a8e1c2636e2242b4aa91012c77d255332f9722573042e9a60e3678c",
    "zh:5f4ba2813c920f6e5fa14bc062f17de16572542772e84f36923a5081365c5045",
    "zh:7993e5330ee2ce1d1d3f9dd4333b109c03ab864e57750bf56655f4b5dabd02be",
    "zh:8a1d4fea4b51dec648f9af471c8fa1e2a57c725f58a83e5d3222755899d8f4fa",
    "zh:9dee734e4342e2ab085d5096f85da5b0fa29eed21aaa87bbf83a1bb663dad537",
    "zh:c99176a828cf4e08b2f85bd48bcea892dea4acd8403402843df3999769e8c92c",
    "zh:d0661e56e6181e5dc588498c32fb7141f9d43041d5158b572fac9f4095edd3e7",
    "zh:e2c362a35be40a38bb6ab574e419a68d220f0e45865968f976c6bf952ac2dc6a",
    "zh:e4fb667e2b6ce970e3566b192a48a59f3f7cc8ed24be5fa7bb7ce77a5bb73398",
    "zh:fd74859a8679c4b1f0a70c4b8d3d4012e3be4fa3403341af2c35533f305e3366",
  ]
}
