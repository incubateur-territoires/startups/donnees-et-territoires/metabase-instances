locals {
  common_config = {
    project_slug = local.project_slug
    dns_root     = local.dns_root
    tem_config = {
      user     = scaleway_tem_domain.metabase.smtps_auth_user
      password = var.tem_api_key
      host     = scaleway_tem_domain.metabase.smtp_host
      port     = scaleway_tem_domain.metabase.smtps_port_alternative
      security = "ssl"
      from     = "noreply@${scaleway_tem_domain.metabase.name}"
    }
  }
}

module "metabase_test" {
  source        = "./metabase"
  team_name     = "Test"
  team_slug     = "test"
  common_config = local.common_config
}

module "metabase_france_services" {
  source        = "./metabase"
  team_name     = "France services"
  team_slug     = "franceservices"
  common_config = local.common_config
}

# pole europe de l'ANCT (https://www.europe-en-france.gouv.fr/fr/synergie-loutil-partage-de-suivi-et-de-gestion-des-fonds-europeens)
module "metabase_synergie" {
  source        = "./metabase"
  team_name     = "Synergie"
  team_slug     = "synergie"
  common_config = local.common_config
}

module "metabase_espace_sur_demande" {
  source        = "./metabase"
  team_name     = "Espace sur demande"
  team_slug     = "espacesurdemande"
  common_config = local.common_config
}

module "metabase_agents_en_intervention" {
  source        = "./metabase"
  team_name     = "Agents en intervention"
  team_slug     = "agentsenintervention"
  common_config = local.common_config
}
