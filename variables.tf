variable "scaleway_project_config" {
  type = object({
    project_id = string
    access_key = string
    secret_key = string
  })
}
variable "tem_api_key" {
  type = string
}

locals {
  project_slug = "donnees"
  dns_root     = "metabase.donnees.incubateur.anct.gouv.fr"
}
