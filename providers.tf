provider "scaleway" {
  region     = "fr-par"
  zone       = "fr-par-1"
  project_id = var.scaleway_project_config.project_id
  access_key = var.scaleway_project_config.access_key
  secret_key = var.scaleway_project_config.secret_key
}
