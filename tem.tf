resource "scaleway_tem_domain" "metabase" {
  name       = local.dns_root
  accept_tos = true
}
