resource "scaleway_domain_record" "metabase" {
  dns_zone = var.common_config.dns_root
  name     = var.team_slug
  type     = "ALIAS"
  data     = "${scaleway_container.metabase.domain_name}."
}
resource "scaleway_container_domain" "metabase" {
  container_id = scaleway_container.metabase.id
  hostname     = scaleway_domain_record.metabase.fqdn
}
