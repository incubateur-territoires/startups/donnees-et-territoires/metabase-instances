variable "common_config" {
  type = object({
    project_slug = string
    dns_root     = string
    tem_config = object({
      user     = string
      password = string
      host     = string
      port     = string
      security = string
      from     = string
    })
  })
}

variable "team_name" {
  type        = string
  description = "Name of the team, used to customize notification emails for example"
}

variable "team_slug" {
  type        = string
  description = "Slug used to identitfy the team for which the Metabase instance is deployed. Used for the domain name"
}

variable "image_registry" {
  type        = string
  default     = "docker.io/metabase/metabase"
  description = "OCI registry to fetch the Metabase image from"
}

variable "image_tag" {
  type    = string
  default = "v0.49.1"
}
