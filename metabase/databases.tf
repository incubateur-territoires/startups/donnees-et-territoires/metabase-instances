resource "scaleway_rdb_instance" "metabase" {
  name              = "${var.common_config.project_slug}-metabase-${var.team_slug}"
  engine            = "PostgreSQL-15"
  node_type         = "db-dev-s"
  is_ha_cluster     = true
  volume_size_in_gb = 5
  volume_type       = "bssd"
}

# Database for the Metabase application
resource "scaleway_rdb_database" "metabase" {
  instance_id = scaleway_rdb_instance.metabase.id
  name        = var.team_slug
}
resource "scaleway_rdb_user" "metabase" {
  instance_id = scaleway_rdb_instance.metabase.id
  name        = var.team_slug
  password    = random_password.metabase_db_user.result
}
resource "random_password" "metabase_db_user" {
  length = 64
}
resource "scaleway_rdb_privilege" "metabase_db_user" {
  instance_id   = scaleway_rdb_instance.metabase.id
  user_name     = scaleway_rdb_user.metabase.name
  database_name = scaleway_rdb_database.metabase.name
  permission    = "all"
}

# Database for users to upload data to
resource "scaleway_rdb_database" "data" {
  instance_id = scaleway_rdb_instance.metabase.id
  name        = "${var.team_slug}-data"
}
resource "scaleway_rdb_user" "data" {
  instance_id = scaleway_rdb_instance.metabase.id
  name        = "${var.team_slug}-data"
  password    = random_password.data_user.result
}
resource "random_password" "data_user" {
  length = 64
}
resource "scaleway_rdb_privilege" "data_user" {
  instance_id   = scaleway_rdb_instance.metabase.id
  user_name     = scaleway_rdb_user.data.name
  database_name = scaleway_rdb_database.data.name
  permission    = "all"
}

resource "null_resource" "rdb_data_config" {
  triggers = {
    host     = scaleway_rdb_instance.metabase.load_balancer[0].ip
    port     = scaleway_rdb_instance.metabase.load_balancer[0].port
    dbname   = scaleway_rdb_database.data.name
    user     = scaleway_rdb_user.data.name
    password = scaleway_rdb_user.data.password
    uri      = local.db_connection_uri_data
  }
}

locals {
  db_connection_uri_fmt = "jdbc:postgresql://${scaleway_rdb_instance.metabase.load_balancer[0].ip}:${scaleway_rdb_instance.metabase.load_balancer[0].port}/%s?user=%s&password=%s&ssl=true&sslmode=require"
  db_connection_uri_mb = format(
    local.db_connection_uri_fmt,
    scaleway_rdb_database.metabase.name,
    urlencode(scaleway_rdb_user.metabase.name),
    urlencode(scaleway_rdb_user.metabase.password)
  )
  db_connection_uri_data = format(
    local.db_connection_uri_fmt,
    scaleway_rdb_database.data.name,
    urlencode(scaleway_rdb_user.data.name),
    urlencode(scaleway_rdb_user.data.password)
  )
}
