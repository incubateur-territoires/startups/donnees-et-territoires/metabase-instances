
resource "scaleway_container_namespace" "metabase" {
  name = "${var.common_config.project_slug}-metabase-${var.team_slug}"
}
resource "scaleway_container" "metabase" {
  namespace_id   = scaleway_container_namespace.metabase.id
  name           = "metabase"
  registry_image = "${var.image_registry}:${var.image_tag}"
  port           = 3000
  environment_variables = {
    MB_DB_TYPE               = "postgres"
    MB_EMAIL_SMTP_HOST       = var.common_config.tem_config.host
    MB_EMAIL_SMTP_PORT       = var.common_config.tem_config.port
    MB_EMAIL_SMTP_SECURITY   = var.common_config.tem_config.security
    MB_EMAIL_FROM_NAME       = "Metabase ${var.team_name}"
    MB_EMAIL_FROM_ADDRESS    = var.common_config.tem_config.from
    MB_SITE_LOCALE           = "fr"
    MB_SITE_NAME             = "Metabase ${var.team_name}"
    MB_ADMIN_EMAIL           = "donnees@anct.gouv.fr"
    MB_ANON_TRACKING_ENABLED = false
  }
  secret_environment_variables = {
    MB_DB_CONNECTION_URI   = local.db_connection_uri_mb
    MB_EMAIL_SMTP_USERNAME = var.common_config.tem_config.user
    MB_EMAIL_SMTP_PASSWORD = var.common_config.tem_config.password
  }
  cpu_limit    = 2240
  memory_limit = 2048
  deploy       = true
  min_scale    = 1
  max_scale    = 1
  http_option  = "redirected"
}
