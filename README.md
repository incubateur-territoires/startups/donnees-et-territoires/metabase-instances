# Provisionning infrastructure with Terraform

This project uses Terraform to provision its infrastructure on Scaleway.
Terraform isn't used to deploy each application update, this is done separately by updating just the container image.

It is recommended to use Terraform 1.5.x.

## Running Terraform locally

- Clone the repository
- Go to GitLab's [repository Terraform states](https://gitlab.com/incubateur-territoires/startups/donnees-et-territoires/metabase-instances/-/terraform)
- Under Actions, copy the Terraform init command and run it after providing your [Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens)
- Save the value of the [CI variable](https://gitlab.com/incubateur-territoires/startups/donnees-et-territoires/metabase-instances/-/settings/ci_cd) `VAR_FILE` under `terraform.tfvars`
- Run `terraform plan -out=plan.tfplan`
- Check the proposed changes, then run `terraform apply plan.tfplan`

## Fetching credentials for the additionnal database for analytics data

The credentials are configured in a `null_resource` as a workaround to be able to fetch them from the state.

```bash
terraform state pull | jq '.resources[] | select( .type=="null_resource" and .name=="rdb_data_config" ) | .instances[].attributes.triggers'
```
