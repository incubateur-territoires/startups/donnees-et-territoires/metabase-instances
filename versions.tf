terraform {
  # latest open source version of terraform
  required_version = "~> 1.5.7"
  required_providers {
    scaleway = {
      source  = "scaleway/scaleway"
      version = "~> 2.38"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.6"
    }
  }
}
